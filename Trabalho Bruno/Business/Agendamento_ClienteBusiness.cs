﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Business
{
    class Agendamento_ClienteBusiness
    {
        Database.Agendamento_ClienteDatabase agendamentoClienteDatabase = new Database.Agendamento_ClienteDatabase();

        public Model.Agendamento_ClienteModel FiltrarPorId(int id)
        {
            Model.Agendamento_ClienteModel model = agendamentoClienteDatabase.FiltrarPorId(id);

            return model;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorNome(string cliente)
        {
            List<Model.Agendamento_ClienteModel> lista = agendamentoClienteDatabase.ConsultarPorNome(cliente);

            return lista;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorCPF (string cpf)
        {
            List<Model.Agendamento_ClienteModel> lista = agendamentoClienteDatabase.ConsultarPorCPF(cpf);

            return lista;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorData(DateTime data)
        {
            List<Model.Agendamento_ClienteModel> lista = agendamentoClienteDatabase.ConsultarPorData(data);

            return lista;
        }
    }
}
