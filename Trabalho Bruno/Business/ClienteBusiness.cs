﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Business
{
    class ClienteBusiness
    {
        Database.ClienteDatabase clienteDatabase = new Database.ClienteDatabase();

        public void InserirCliente(Model.ClienteModel model)
        {
            if(model.cliente == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatorio");
            }

            clienteDatabase.InserirCliente(model);
        }

        public List<Model.ClienteModel> Listar()
        {
            List<Model.ClienteModel> lista = clienteDatabase.Listar();

            return lista;
        }

        public List<Model.ClienteModel> FiltrarPorNome(string nome)
        {
            List <Model.ClienteModel> lista = clienteDatabase.FiltrarPorNome(nome);

            return lista;
        }

        public Model.ClienteModel BuscarPorNome(string nome)
        {
            Model.ClienteModel model = clienteDatabase.BuscarPorNome(nome);

            return model;
        }

        public List<Model.ClienteModel> FiltrarPorCPF(string cpf)
        {
            List<Model.ClienteModel> lista = clienteDatabase.FiltrarPorCPF(cpf);

            return lista;
        }
        public void alterarcliente (Model.ClienteModel model)
        {
            if (model.cliente == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatorio");
            }

            Database.ClienteDatabase clientedatabase = new Database.ClienteDatabase();
            clienteDatabase.alterarCliente(model);
        }
        public Model.ClienteModel BuscarId(int id)
        {
            Database.ClienteDatabase Database = new Database.ClienteDatabase();
            Model.ClienteModel Modelo = Database.BuscarId(id);
            return Modelo;
        }
        public void Deletar(int id)
        {
            Database.ClienteDatabase Database = new Database.ClienteDatabase();
            Database.Deletar(id);
        }
    }
}
