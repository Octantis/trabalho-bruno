﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Business
{
    class AgendamentoBusiness
    {
        Database.AgendamentoDatabase agendamentoDatabase = new Database.AgendamentoDatabase();

        public void InserirAgendamento(Model.AgendamentoModel model)
        {
            if(model.chegada == null)
            {
                throw new ArgumentException("Horario de chegada obrigatorio");
            }

            if (model.saida == null)
            {
                throw new ArgumentException("Horario de saída obrigatorio");
            }

            if(model.convidados == 0)
            {
                throw new ArgumentException("Número de convidados obrigatorio");
            }

            agendamentoDatabase.InserirAgendamento(model);
        }

        public void alterarAgendamento(Model.AgendamentoModel model)
        {

            if (model.chegada == null)
            {
                throw new ArgumentException("Horario de chegada obrigatorio");
            }

            if (model.saida == null)
            {
                throw new ArgumentException("Horario de saída obrigatorio");
            }

            if (model.convidados == 0)
            {
                throw new ArgumentException("Número de convidados obrigatorio");
            }

            agendamentoDatabase.alterarAgendamento(model);
        }
        public Model.AgendamentoModel FiltrarPorId(int id)
        {
            Model.AgendamentoModel model = agendamentoDatabase.FiltrarPorId(id);

            return model;
        }

        public void DeletarAgendamento(int id)
        {
            agendamentoDatabase.DeletarAgendamento(id);
        }
    }
}
