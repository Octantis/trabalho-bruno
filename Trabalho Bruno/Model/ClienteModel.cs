﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Model
{
    public class ClienteModel
    {
        public int IDCliente { get; set; }
        public string cliente { get; set; }
        public string cpf { get; set; }
        public string email { get; set; }
        public DateTime nasc { get; set; }
        public string tel { get; set; }
        public string celular { get; set; }
    }
}
