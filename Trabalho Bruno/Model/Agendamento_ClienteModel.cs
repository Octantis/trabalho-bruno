﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Model
{
    public class Agendamento_ClienteModel
    {
        public int IDAgendamento { get; set; }
        public int IDCliente { get; set; }

        public string cliente { get; set; }
        public string cpf { get; set; }
        public string email { get; set; }
        public DateTime nasc { get; set; }
        public string tel { get; set; }
        public string celular { get; set; }

        public DateTime dataInicio { get; set; }
        public DateTime dataFim { get; set; }
        public TimeSpan chegada { get; set; }
        public TimeSpan saida { get; set; }

        public int convidados { get; set; }
        public bool pulapula { get; set; }
        public bool piscina { get; set; }
        public bool tirolesa { get; set; }
        public bool volei { get; set; }

        public decimal valorTotal { get; set; }
    }
}
