﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Database
{
    class Agendamento_ClienteDatabase
    {
        public Model.Agendamento_ClienteModel FiltrarPorId(int id)
        {

            string script = @"select *
                                from tb_agendamento
                                join tb_cliente
                                on   tb_agendamento.id_cliente = tb_cliente.id_cliente
                                where id_agendamento    = @id_agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agendamento", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.Agendamento_ClienteModel model = null;

            if (reader.Read())
            {
                model = new Model.Agendamento_ClienteModel();

                model.IDAgendamento = reader.GetInt32("id_agendamento");
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.email = reader.GetString("ds_email");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");
                model.dataInicio = reader.GetDateTime("dt_dataInicio");
                model.dataFim = reader.GetDateTime("dt_dataFim");
                model.chegada = reader.GetTimeSpan("hr_chegada");
                model.saida = reader.GetTimeSpan("hr_saida");
                model.convidados = reader.GetInt32("qt_convidados");
                model.pulapula = reader.GetBoolean("bt_pulapula");
                model.piscina = reader.GetBoolean("bt_piscina");
                model.tirolesa = reader.GetBoolean("bt_tirolesa");
                model.volei = reader.GetBoolean("bt_volei");
                model.valorTotal = reader.GetDecimal("vl_valorTotal");
            }
            reader.Close();

            return model;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorNome(string cliente)
        {
            string script = @"select *
                                from tb_agendamento
                                join tb_cliente
                                on   tb_agendamento.id_cliente = tb_cliente.id_cliente
                                where nm_cliente    like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + cliente + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.Agendamento_ClienteModel> lista = new List<Model.Agendamento_ClienteModel>();

            while (reader.Read())
            {
                Model.Agendamento_ClienteModel model = new Model.Agendamento_ClienteModel();
                model.IDAgendamento = reader.GetInt32("id_agendamento");
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.email = reader.GetString("ds_email");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");
                model.dataInicio = reader.GetDateTime("dt_dataInicio");
                model.dataFim = reader.GetDateTime("dt_dataFim");
                model.chegada = reader.GetTimeSpan("hr_chegada");
                model.saida = reader.GetTimeSpan("hr_saida");
                model.convidados = reader.GetInt32("qt_convidados");
                model.pulapula = reader.GetBoolean("bt_pulapula");
                model.piscina = reader.GetBoolean("bt_piscina");
                model.tirolesa = reader.GetBoolean("bt_tirolesa");
                model.volei = reader.GetBoolean("bt_volei");
                model.valorTotal = reader.GetDecimal("vl_valorTotal");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorCPF(string cpf)
        {
            string script = @"select *
                                from tb_agendamento
                                join tb_cliente
                                on   tb_agendamento.id_cliente = tb_cliente.id_cliente
                                where ds_cpf    like @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", "%" + cpf + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.Agendamento_ClienteModel> lista = new List<Model.Agendamento_ClienteModel>();

            while (reader.Read())
            {

                Model.Agendamento_ClienteModel model = new Model.Agendamento_ClienteModel();
                model.IDAgendamento = reader.GetInt32("id_agendamento");
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.email = reader.GetString("ds_email");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");
                model.dataInicio = reader.GetDateTime("dt_dataInicio");
                model.dataFim = reader.GetDateTime("dt_dataFim");
                model.chegada = reader.GetTimeSpan("hr_chegada");
                model.saida = reader.GetTimeSpan("hr_saida");
                model.convidados = reader.GetInt32("qt_convidados");
                model.pulapula = reader.GetBoolean("bt_pulapula");
                model.piscina = reader.GetBoolean("bt_piscina");
                model.tirolesa = reader.GetBoolean("bt_tirolesa");
                model.volei = reader.GetBoolean("bt_volei");
                model.valorTotal = reader.GetDecimal("vl_valorTotal");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public List<Model.Agendamento_ClienteModel> ConsultarPorData(DateTime data)
        {
            string script = @"select * from tb_agendamento 
                                            join tb_cliente 
                                            on tb_agendamento.id_cliente = tb_cliente.id_cliente 
                                            where @dt_dataInicio between dt_dataInicio and dt_dataFim;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_dataInicio", data));
                
            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.Agendamento_ClienteModel> lista = new List<Model.Agendamento_ClienteModel>();

            while (reader.Read())
            {
                Model.Agendamento_ClienteModel model = new Model.Agendamento_ClienteModel();
                model.IDAgendamento = reader.GetInt32("id_agendamento");
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.email = reader.GetString("ds_email");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");
                model.dataInicio = reader.GetDateTime("dt_dataInicio");
                model.dataFim = reader.GetDateTime("dt_dataFim");
                model.chegada = reader.GetTimeSpan("hr_chegada");
                model.saida = reader.GetTimeSpan("hr_saida");
                model.convidados = reader.GetInt32("qt_convidados");
                model.pulapula = reader.GetBoolean("bt_pulapula");
                model.piscina = reader.GetBoolean("bt_piscina");
                model.tirolesa = reader.GetBoolean("bt_tirolesa");
                model.volei = reader.GetBoolean("bt_volei");
                model.valorTotal = reader.GetDecimal("vl_valorTotal");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }
    }
}
