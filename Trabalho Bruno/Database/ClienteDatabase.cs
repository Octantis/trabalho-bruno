﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Trabalho_Bruno.Database
{
    class ClienteDatabase
    {
        public void Deletar(int id)
        {
            string script = @"delete from tb_cliente where id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
        public Model.ClienteModel BuscarId(int id)
        {
            string script = "select * from tb_cliente where id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.ClienteModel Modelo = null;

            if (reader.Read())
            {
                Modelo = new Model.ClienteModel();
                Modelo.IDCliente = Convert.ToInt32(reader["id_cliente"]);
                Modelo.cliente = Convert.ToString(reader["nm_cliente"]);
                Modelo.cpf = Convert.ToString(reader["ds_cpf"]);
                Modelo.email = Convert.ToString(reader["ds_email"]);
                Modelo.nasc = Convert.ToDateTime(reader["dt_nasc"]);
                Modelo.tel = Convert.ToString(reader["ds_tel"]);
                Modelo.celular = Convert.ToString(reader["ds_celular"]);
            }
            reader.Close();

            return Modelo;
        }
        public void InserirCliente(Model.ClienteModel espacosampaio)
        {
            string script = @"insert into tb_cliente (nm_cliente, ds_cpf, ds_email, dt_nasc, ds_tel, ds_celular)
                                             values (@nm_cliente, @ds_cpf, @ds_email, @dt_nasc, @ds_tel, @ds_celular)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", espacosampaio.cliente));
            parms.Add(new MySqlParameter("ds_cpf", espacosampaio.cpf));
            parms.Add(new MySqlParameter("ds_email", espacosampaio.email));
            parms.Add(new MySqlParameter("dt_nasc", espacosampaio.nasc));
            parms.Add(new MySqlParameter("ds_tel", espacosampaio.tel));
            parms.Add(new MySqlParameter("ds_celular", espacosampaio.celular));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.ClienteModel> Listar()
        {
            string script = @"select * from tb_cliente order by nm_cliente";

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<Model.ClienteModel> lista = new List<Model.ClienteModel>();

            while (reader.Read())
            {
                Model.ClienteModel model = new Model.ClienteModel();
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.email = reader.GetString("ds_email");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public List<Model.ClienteModel> FiltrarPorNome(string nome)
        {
            string script = @"select * from tb_cliente where nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + nome + "%"));

            DB db = new Database.DB();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ClienteModel> lista = new List<Model.ClienteModel>();

            while (reader.Read())
            {
                Model.ClienteModel model = new Model.ClienteModel();
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.email = reader.GetString("ds_email");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public Model.ClienteModel BuscarPorNome(string nome)
        {
            string script = @"select * from tb_cliente where nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + nome + "%"));

            DB db = new Database.DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.ClienteModel model = null;

            if (reader.Read())
            {
                model = new Model.ClienteModel();
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.email = reader.GetString("ds_email");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");
            }
            reader.Close();

            return model;
        }

        public List<Model.ClienteModel> FiltrarPorCPF(string cpf)
        {
            string script = @"select * from tb_cliente where ds_cpf like @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", "%" + cpf + "%"));

            DB db = new Database.DB();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ClienteModel> lista = new List<Model.ClienteModel>();

            while (reader.Read())
            {
                Model.ClienteModel model = new Model.ClienteModel();
                model.IDCliente = reader.GetInt32("id_cliente");
                model.cliente = reader.GetString("nm_cliente");
                model.cpf = reader.GetString("ds_cpf");
                model.nasc = reader.GetDateTime("dt_nasc");
                model.email = reader.GetString("ds_email");
                model.tel = reader.GetString("ds_tel");
                model.celular = reader.GetString("ds_celular");

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public void alterarCliente(Model.ClienteModel model)
        {
            string script = @"update tb_cliente set nm_cliente = @nm_cliente,
                                                    ds_cpf = @ds_cpf,
                                                    dt_nasc = @dt_nasc,
                                                    ds_email = @ds_email,
                                                    ds_tel = @ds_tel,
                                                    ds_celular = @ds_celular where id_cliente = @id_cliente";


            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("nm_cliente", model.cliente));
            parms.Add(new MySqlParameter("ds_cpf", model.cpf));
            parms.Add(new MySqlParameter("dt_nasc", model.nasc));
            parms.Add(new MySqlParameter("ds_email", model.email));
            parms.Add(new MySqlParameter("ds_tel", model.tel));
            parms.Add(new MySqlParameter("ds_celular", model.celular));
            parms.Add(new MySqlParameter("id_cliente", model.IDCliente));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    } 
}
