﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Bruno.Database
{
    class AgendamentoDatabase
    {
            public void InserirAgendamento(Model.AgendamentoModel agendamento)
        {
            string script = @"insert into tb_agendamento    
                                    (
                                     id_cliente,
                                     dt_dataInicio, 
                                     dt_dataFim, 
                                     hr_chegada, 
                                     hr_saida,
                                     qt_convidados, 
                                     bt_pulapula, 
                                     bt_piscina, 
                                     bt_tirolesa, 
                                     bt_volei, 
                                     vl_valorTotal
                                   )
                                    values
                                   (   
                                    @id_cliente,
                                    @dt_dataInicio, 
                                    @dt_dataFim, 
                                    @hr_chegada, 
                                    @hr_saida, 
                                    @qt_convidados,
                                    @bt_pulapula, 
                                    @bt_piscina, 
                                    @bt_tirolesa, 
                                    @bt_volei, 
                                    @vl_valorTotal
                                   )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_dataInicio", agendamento.dataInicio));
            parms.Add(new MySqlParameter("dt_dataFim", agendamento.dataFim));
            parms.Add(new MySqlParameter("hr_chegada", agendamento.chegada));
            parms.Add(new MySqlParameter("hr_saida", agendamento.saida));
            parms.Add(new MySqlParameter("qt_convidados", agendamento.convidados));
            parms.Add(new MySqlParameter("bt_pulapula", agendamento.pulapula));
            parms.Add(new MySqlParameter("bt_piscina", agendamento.piscina));
            parms.Add(new MySqlParameter("bt_tirolesa", agendamento.tirolesa));
            parms.Add(new MySqlParameter("bt_volei", agendamento.volei));
            parms.Add(new MySqlParameter("vl_valorTotal", agendamento.valorTotal));
            parms.Add(new MySqlParameter("id_cliente", agendamento.IDCliente));

            DB db = new Database.DB();
            db.ExecuteInsertScript(script, parms);
        }
        
        public void alterarAgendamento(Model.AgendamentoModel model)
        {
            string script = @"update tb_agendamento 
                                    set dt_dataInicio =     @dt_dataInicio,
                                        dt_dataFim =        @dt_datafim,
                                        hr_chegada =        @hr_chegada,
                                        hr_saida =          @hr_saida,
                                        qt_convidados =     @qt_convidados,
                                        bt_pulapula =       @bt_pulapula,
                                        bt_piscina =        @bt_piscina,
                                        bt_tirolesa =       @bt_tirolesa,
                                        bt_volei =          @bt_volei,
                                        vl_valorTotal =     @vl_valorTotal
                                  where id_agendamento =    @id_agendamento";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_dataInicio", model.dataInicio));
            parms.Add(new MySqlParameter("dt_dataFim", model.dataFim));
            parms.Add(new MySqlParameter("hr_chegada", model.chegada));
            parms.Add(new MySqlParameter("hr_saida", model.saida));
            parms.Add(new MySqlParameter("qt_convidados", model.convidados));
            parms.Add(new MySqlParameter("bt_pulapula", model.pulapula));
            parms.Add(new MySqlParameter("bt_piscina", model.piscina));
            parms.Add(new MySqlParameter("bt_tirolesa", model.tirolesa));
            parms.Add(new MySqlParameter("bt_volei", model.volei));
            parms.Add(new MySqlParameter("vl_valorTotal", model.valorTotal));
            parms.Add(new MySqlParameter("id_agendamento", model.IDAgendamento));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public Model.AgendamentoModel FiltrarPorId(int id)
        {

            string script = @"select * from tb_agendamento where id_agendamento = @id_agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agendamento", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.AgendamentoModel model = null;

            if (reader.Read())
            {
                model = new Model.AgendamentoModel();
                model.IDAgendamento = reader.GetInt32("id_agendamento");
                model.dataInicio = reader.GetDateTime("dt_dataInicio");
                model.dataFim = reader.GetDateTime("dt_dataFim");
                model.chegada = reader.GetTimeSpan("hr_chegada");
                model.saida = reader.GetTimeSpan("hr_saida");
                model.convidados = reader.GetInt32("qt_convidados");
                model.pulapula = reader.GetBoolean("bt_pulapula");
                model.piscina = reader.GetBoolean("bt_piscina");
                model.tirolesa = reader.GetBoolean("bt_tirolesa");
                model.volei = reader.GetBoolean("bt_volei");
                model.valorTotal = reader.GetDecimal("vl_valorTotal");
            }
            reader.Close();

            return model;
        }

        public void DeletarAgendamento(int id)
        {
            string script = @"delete from tb_agendamento where id_agendamento = @id_agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agendamento", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

    }
}
