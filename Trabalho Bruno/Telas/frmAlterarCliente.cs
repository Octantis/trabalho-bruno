﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno.Telas
{
    public partial class frmAlterarCliente : Form
    {
        public frmAlterarCliente()
        {
            InitializeComponent();
        }

        public void CarregarTela(Model.ClienteModel model)
        {
            txtIdCliente.Text = model.IDCliente.ToString();
        }

        private void Alterar()
        {
            try
            {
                Model.ClienteModel model = new Model.ClienteModel();

                model.IDCliente = Convert.ToInt32(txtIdCliente.Text);
                model.email = txtEmail.Text;
                model.cpf = txtCPF.Text;
                model.celular = txtCelular.Text;
                model.tel = txtTelefone.Text;
                model.cliente = txtNome.Text;
                model.nasc = dtpNascimento.Value;

                DateTime nascimento = dtpNascimento.Value;
                DateTime atual = DateTime.Now;
                int idade = atual.Year - nascimento.Year;
                string cpf = txtCPF.Text;

                if (idade < 18)
                {
                    MessageBox.Show("Menor de 18 anos");
                }
                else
                {
                    if (txtNome.Text == string.Empty)
                    {
                        MessageBox.Show("Informe o nome do cliente");
                    }
                    else if (cpf.Length < 14)
                    {
                        MessageBox.Show("CPF invalido");
                    }
                    else if (model.email.Contains("@"))
                    {
                        Business.ClienteBusiness clientebusinese = new Business.ClienteBusiness();
                        clientebusinese.alterarcliente(model);

                        MessageBox.Show("Informações do cliente alteradas com sucesso");
                    }
                    else
                    {
                        MessageBox.Show("Email invalido");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtIdCliente_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtIdCliente.Text);
                Business.ClienteBusiness Business = new Business.ClienteBusiness();
                Model.ClienteModel Modelo = Business.BuscarId(id);

                if (Modelo != null)
                {
                    txtNome.Text = Modelo.cliente;
                    txtEmail.Text = Modelo.email;
                    txtCPF.Text = Modelo.cpf;
                    dtpNascimento.Value = Modelo.nasc;
                    txtTelefone.Text = Modelo.tel;
                    txtCelular.Text = Modelo.celular;
                }
                else
                {
                    txtNome.Text = string.Empty;
                    txtCPF.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    dtpNascimento.Value = DateTime.Now;
                    txtTelefone.Text = string.Empty;
                    txtCelular.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("ID deve ser preenchido");
                txtNome.Text = string.Empty;
                txtCPF.Text = string.Empty;
                txtEmail.Text = string.Empty;
                dtpNascimento.Value = DateTime.Now;
                txtTelefone.Text = string.Empty;
                txtCelular.Text = string.Empty;
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }
    }
}
