﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmAlterarAgendamento : Form
    {
        public frmAlterarAgendamento()
        {
            InitializeComponent();
        }

        public void CarregarTela(Model.Agendamento_ClienteModel model)
        {
            txtID.Text = model.IDAgendamento.ToString();
        }

        private void Alterar()
        {
            try
            {
                if(dtpFim.Value < dtpInicio.Value)
                {
                    MessageBox.Show("Data final não poder ser antes de data de início");
                }
                else
                {
                    if (nudConvidados.Value == 0)
                    {
                        MessageBox.Show("Informe o numero de convidados");
                    }
                    else
                    {
                        Model.AgendamentoModel model = new Model.AgendamentoModel();

                        model.IDAgendamento = Convert.ToInt32(txtID.Text);
                        model.dataInicio = dtpInicio.Value;
                        model.dataFim = dtpFim.Value;
                        model.chegada = TimeSpan.Parse(txtInicio.Text);
                        model.saida = TimeSpan.Parse(txtFim.Text);
                        model.convidados = Convert.ToInt32(nudConvidados.Value);
                        model.piscina = chkPiscina.Checked;
                        model.pulapula = chkPulaPula.Checked;
                        model.tirolesa = chkTirolesa.Checked;
                        model.volei = chkVolei.Checked;
                        model.valorTotal = Convert.ToDecimal(lblValorTotal.Text);

                        Business.AgendamentoBusiness agendamentobusiness = new Business.AgendamentoBusiness();
                        agendamentobusiness.alterarAgendamento(model);

                        MessageBox.Show("Agendamento alterado com sucesso");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CalcularValorTotal()
        {
            try
            {
                bool pulapula = chkPulaPula.Checked;
                bool piscina = chkPiscina.Checked;
                bool tirolesa = chkTirolesa.Checked;
                bool volei = chkVolei.Checked;
                DateTime inicio = dtpInicio.Value;
                DateTime fim = dtpFim.Value;
                TimeSpan dias = fim.AddDays(01) - inicio;

                decimal Total = dias.Days * 850;

                if (pulapula == true)
                {
                    Total = Total + 100;
                }
                if (piscina == true)
                {
                    Total = Total + 150;
                }
                if (tirolesa == true)
                {
                    Total = Total + 50;
                }
                if (volei == true)
                {
                    Total = Total + 30;
                }

                lblValorTotal.Text = Total.ToString();
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int idAgendamento = Convert.ToInt32(txtID.Text);

                Business.Agendamento_ClienteBusiness BusinessAG = new Business.Agendamento_ClienteBusiness();
                Model.Agendamento_ClienteModel Modelo = BusinessAG.FiltrarPorId(idAgendamento);

                if (Modelo != null)
                {
                    txtNomeCli.Text = Modelo.cliente;
                    dtpInicio.Value = Modelo.dataInicio;
                    dtpFim.Value = Modelo.dataFim;
                    txtInicio.Text = Convert.ToString(Modelo.chegada);
                    txtFim.Text = Convert.ToString(Modelo.saida);
                    nudConvidados.Value = Modelo.convidados;
                    chkPulaPula.Checked = Modelo.pulapula;
                    chkPiscina.Checked = Modelo.piscina;
                    chkTirolesa.Checked = Modelo.tirolesa;
                    chkVolei.Checked = Modelo.volei;
                    lblValorTotal.Text = Convert.ToString(Modelo.valorTotal);
                }
                else
                {
                    dtpInicio.Value = DateTime.Now;
                    dtpFim.Value = DateTime.Now;
                    txtInicio.Text = string.Empty;
                    txtFim.Text = string.Empty;
                    nudConvidados.Value = 0;
                    chkPulaPula.Checked = false;
                    chkPiscina.Checked = false;
                    chkTirolesa.Checked = false;
                    chkVolei.Checked = false;
                    lblValorTotal.Text = "0";
                }
            }
            catch
            {
                MessageBox.Show("ID nao pode estar vazio");

                dtpInicio.Value = DateTime.Now;
                dtpFim.Value = DateTime.Now;
                txtInicio.Text = string.Empty;
                txtFim.Text = string.Empty;
                nudConvidados.Value = 0;
                chkPulaPula.Checked = false;
                chkPiscina.Checked = false;
                chkTirolesa.Checked = false;
                chkVolei.Checked = false;
                lblValorTotal.Text = "0";
            }
        }

        private void dtpInicio_onValueChanged(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void dtpFim_onValueChanged(object sender, EventArgs e)
        {
            if(dtpFim.Value < dtpInicio.Value)
            {
                MessageBox.Show("Data final não pode ser menor do que data de início");
            }
            else
            {
                this.CalcularValorTotal();
            }
        }

        private void chkPulaPula_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkPiscina_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkTirolesa_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkVolei_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void btnAlterar_Click_1(object sender, EventArgs e)
        {
            this.Alterar();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNomeCli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtInicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void txtFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void nudConvidados_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void chkPulaPula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void chkPiscina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void chkTirolesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void chkVolei_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Alterar();
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {
            bool pulapula = chkPulaPula.Checked;

            if (pulapula == true)
            {
                chkPulaPula.Checked = false;
            }
            else if (pulapula == false)
            {
                chkPulaPula.Checked = true;
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {
            bool piscina = chkPiscina.Checked;

            if (piscina == true)
            {
                chkPiscina.Checked = false;
            }
            else if (piscina == false)
            {
                chkPiscina.Checked = true;
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            bool tirolesa = chkTirolesa.Checked;

            if (tirolesa == true)
            {
                chkTirolesa.Checked = false;
            }
            else if (tirolesa == false)
            {
                chkTirolesa.Checked = true;
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {
            bool volei = chkVolei.Checked;

            if (volei == true)
            {
                chkVolei.Checked = false;
            }
            else if (volei == false)
            {
                chkVolei.Checked = true;
            }
        }
    }
}
