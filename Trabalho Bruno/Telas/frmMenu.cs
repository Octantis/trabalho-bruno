﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            frmCadastroCliente tela = new Trabalho_Bruno.frmCadastroCliente();
            tela.Show();
        }

        private void btnAgendar_Click(object sender, EventArgs e)
        {
            frmAgendamento tela = new frmAgendamento();
            tela.Show();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            frmConsultarCliente tela = new frmConsultarCliente();
            tela.Show();
        }

        private void btnBuscarAgendamento_Click(object sender, EventArgs e)
        {
            frmConsulta tela = new frmConsulta();
            tela.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
