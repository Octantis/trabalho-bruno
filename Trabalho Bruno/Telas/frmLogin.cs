﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Entrar()
        {
            string usuario = txtUsuario.Text;
            string senha = txtSenha.Text;

            if (usuario != "Sampaio")
            {
                MessageBox.Show("Usuario incorreto");
            }
            else if (usuario == "Sampaio" && senha != "12345678")
            {
                MessageBox.Show("Senha incorreta");
            }
            else
            {
                Telas.frmMenu menu = new Telas.frmMenu();
                menu.Show();
                this.Hide();
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            this.Entrar();
        }

        private void btnEntrar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Entrar();
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Entrar();
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Entrar();
            }
        }
    }
}
