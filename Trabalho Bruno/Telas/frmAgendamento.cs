﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmAgendamento : Form
    {
        public frmAgendamento()
        {
            InitializeComponent();

            this.CarregarCliente();
        }


        Model.AgendamentoModel agendamento = new Model.AgendamentoModel();

        private void Cadastrar()
        {
            try
            {
                if(dtpInicio.Value < DateTime.Now)
                {
                    MessageBox.Show("Data de início muito antiga");
                }
                else
                {
                    if(dtpFim.Value < dtpInicio.Value)
                    {
                        MessageBox.Show("Data final não pode ser antes da data de início");
                    }
                    else
                    {
                        if (nudConvidados.Value == 0)
                        {
                            MessageBox.Show("Informe o numero de convidados");
                        }
                        else
                        {
                            Model.ClienteModel cliente = cboCliente.SelectedItem as Model.ClienteModel;

                            TimeSpan chegada = TimeSpan.Parse(txtInicio.Text);
                            TimeSpan saida = TimeSpan.Parse(txtFim.Text);
                            agendamento.dataInicio = dtpInicio.Value.Date;
                            agendamento.dataFim = dtpFim.Value.Date;
                            agendamento.chegada = chegada;
                            agendamento.saida = saida;
                            agendamento.convidados = Convert.ToInt32(nudConvidados.Value);
                            agendamento.pulapula = chkPulaPula.Checked;
                            agendamento.piscina = chkPiscina.Checked;
                            agendamento.tirolesa = chkTirolesa.Checked;
                            agendamento.volei = chkVolei.Checked;
                            agendamento.valorTotal = Convert.ToDecimal(lblValorTotal.Text);
                            agendamento.IDCliente = cliente.IDCliente;

                            Business.AgendamentoBusiness agendamentobusiness = new Business.AgendamentoBusiness();
                            agendamentobusiness.InserirAgendamento(agendamento);

                            MessageBox.Show("Agendamento cadastrado com sucesso");
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CarregarCliente()
        {
            try
            {
                Business.ClienteBusiness clienteBusiness = new Business.ClienteBusiness();
                List<Model.ClienteModel> lista = clienteBusiness.Listar();

                cboCliente.DisplayMember = nameof(Model.ClienteModel.cliente);
                cboCliente.DataSource = lista;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CalcularValorTotal()
        {
            try
            {
                bool pulapula = chkPulaPula.Checked;
                bool piscina = chkPiscina.Checked;
                bool tirolesa = chkTirolesa.Checked;
                bool volei = chkVolei.Checked;
                DateTime inicio = dtpInicio.Value;
                DateTime fim = dtpFim.Value;
                TimeSpan dias = fim.AddDays(01) - inicio;

                decimal Total = dias.Days * 850;

                if (pulapula == true)
                {
                    Total = Total + 100;
                }
                if (piscina == true)
                {
                    Total = Total + 150;
                }
                if (tirolesa == true)
                {
                    Total = Total + 50;
                }
                if (volei == true)
                {
                    Total = Total + 30;
                }

                lblValorTotal.Text = Total.ToString();
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }

        }

        private void CarregarEmail()
        {
            try
            {
                string cliente = cboCliente.Text;

                Business.ClienteBusiness clientebusiness = new Business.ClienteBusiness();
                Model.ClienteModel model = clientebusiness.BuscarPorNome(cliente);

                if (model != null)
                {
                    txtEmail.Text = model.email;
                }
                else
                {
                    txtEmail.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void cboCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarEmail();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkPulaPula_MouseEnter_1(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Pulapula;
        }

        private void label12_MouseEnter(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Pulapula;
        }

        private void chkPiscina_MouseEnter_1(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Piscina;
        }

        private void label11_MouseEnter(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Piscina;
        }

        private void chkTirolesa_MouseEnter_1(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Tirolesa;
        }

        private void label13_MouseEnter(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.Tirolesa;
        }

        private void chkVolei_MouseEnter_1(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.voleinagrama;
        }

        private void label14_MouseEnter(object sender, EventArgs e)
        {
            imgOpcoes.BackgroundImage = Properties.Resources.voleinagrama;
        }

        private void chkPulaPula_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkPiscina_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkTirolesa_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void chkVolei_OnChange(object sender, EventArgs e)
        {
            this.CalcularValorTotal();
        }

        private void dtpInicio_onValueChanged(object sender, EventArgs e)
        {
            if(dtpInicio.Value < DateTime.Now)
            {
                MessageBox.Show("Data muito antiga");
            }
            else
            {
                this.CalcularValorTotal();
            }
        }

        private void dtpFim_onValueChanged(object sender, EventArgs e)
        {
            if(dtpFim.Value < dtpInicio.Value)
            {
                MessageBox.Show("Data final menor do que a data de ínicio");
            }
            else
            {
                this.CalcularValorTotal();
            }
        }

        private void btnCadastrar_Click_1(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        private void cboCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void dtpInicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void dtpFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtInicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void nudConvidados_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void btnCadastrar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {
            bool pulapula = chkPulaPula.Checked;

            if(pulapula == true)
            {
                chkPulaPula.Checked = false;
            }
            else if(pulapula == false)
            {
                chkPulaPula.Checked = true;
            }
        }

        private void chkPulaPula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void chkTirolesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void chkPiscina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void chkVolei_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {
            bool piscina = chkPiscina.Checked;

            if(piscina == true)
            {
                chkPiscina.Checked = false;
            }
            else if(piscina == false)
            {
                chkPiscina.Checked = true;
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            bool tirolesa = chkTirolesa.Checked;

            if (tirolesa == true)
            {
                chkTirolesa.Checked = false;
            }
            else if (tirolesa == false)
            {
                chkTirolesa.Checked = true;
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {
            bool volei = chkVolei.Checked;

            if (volei == true)
            {
                chkVolei.Checked = false;
            }
            else if (volei == false)
            {
                chkVolei.Checked = true;
            }
        }
    }
}
