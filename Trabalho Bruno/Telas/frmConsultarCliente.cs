﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno.Telas
{
    public partial class frmConsultarCliente : Form
    {
        public frmConsultarCliente()
        {
            InitializeComponent();
        }

        private void ProcessoConsultaPorNome()
        {
            try
            {
                string nome = txtCliente.Text;

                Business.ClienteBusiness clientebusiness = new Business.ClienteBusiness();
                List<Model.ClienteModel> lista = clientebusiness.FiltrarPorNome(nome);

                dgvLista.AutoGenerateColumns = false;
                dgvLista.DataSource = lista;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void ProcessoConsultaPorCPF()
        {
            try
            {
                string cpf = txtCPF.Text;

                Business.ClienteBusiness clientebusiness = new Business.ClienteBusiness();
                List<Model.ClienteModel> lista = clientebusiness.FiltrarPorCPF(cpf);

                dgvLista.AutoGenerateColumns = false;
                dgvLista.DataSource = lista;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {
            this.ProcessoConsultaPorNome();
        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {
            this.ProcessoConsultaPorCPF();
        }

        private void dgvLista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    Model.ClienteModel model = dgvLista.CurrentRow.DataBoundItem as Model.ClienteModel;

                    frmAlterarCliente alterar = new frmAlterarCliente();
                    alterar.CarregarTela(model);
                    alterar.ShowDialog();
                }
                if (e.ColumnIndex == 1)
                {
                    Model.ClienteModel model = dgvLista.CurrentRow.DataBoundItem as Model.ClienteModel;

                    frmDeletarCliente deletar = new frmDeletarCliente();
                    deletar.CarregarTela(model);
                    deletar.ShowDialog();
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.ProcessoConsultaPorNome();
            }
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.ProcessoConsultaPorCPF();
            }
        }
    }
}
