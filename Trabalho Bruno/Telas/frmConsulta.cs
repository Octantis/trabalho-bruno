﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmConsulta : Form
    {
        public frmConsulta()
        {
            InitializeComponent();
        }

        private void ProcessoConsultaPorNome()
        {
            Model.Agendamento_ClienteModel ClienteDados = new Model.Agendamento_ClienteModel();

            string cliente = txtCliente.Text;

            Business.Agendamento_ClienteBusiness businessCliente = new Business.Agendamento_ClienteBusiness();
            List<Model.Agendamento_ClienteModel> lista = businessCliente.ConsultarPorNome(cliente);

            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = lista;
        }

        private void ProcessoConsultaPorCPF()
        {
            Model.Agendamento_ClienteModel ClienteDados = new Model.Agendamento_ClienteModel();

            string cpf = txtCPF.Text;

            Business.Agendamento_ClienteBusiness businessCliente = new Business.Agendamento_ClienteBusiness();
            List<Model.Agendamento_ClienteModel> lista = businessCliente.ConsultarPorCPF(cpf);

            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = lista;
        }

        private void ProcessoConsultaPorData()
        {
            Model.Agendamento_ClienteModel ClienteDados = new Model.Agendamento_ClienteModel();
           
            DateTime data = dtpData.Value.Date;

            Business.Agendamento_ClienteBusiness businessCliente = new Business.Agendamento_ClienteBusiness();
            List<Model.Agendamento_ClienteModel> lista = businessCliente.ConsultarPorData(data);

            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                Model.Agendamento_ClienteModel model = dgvConsultar.CurrentRow.DataBoundItem as Model.Agendamento_ClienteModel;

                frmAlterarAgendamento alterar = new Trabalho_Bruno.frmAlterarAgendamento();
                alterar.CarregarTela(model);
                alterar.ShowDialog();
            }
            if (e.ColumnIndex == 1)
            {
                Model.Agendamento_ClienteModel model = dgvConsultar.CurrentRow.DataBoundItem as Model.Agendamento_ClienteModel;

                frmDeletarAgendamento deletar = new Trabalho_Bruno.frmDeletarAgendamento();
                deletar.CarregarTela(model);
                deletar.ShowDialog();
            }
        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {
            this.ProcessoConsultaPorCPF();
        }

        private void dtpData_onValueChanged(object sender, EventArgs e)
        {
            this.ProcessoConsultaPorData();
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {
            this.ProcessoConsultaPorNome();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.ProcessoConsultaPorNome();
            }
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.ProcessoConsultaPorCPF();
            }
        }
    }
}
