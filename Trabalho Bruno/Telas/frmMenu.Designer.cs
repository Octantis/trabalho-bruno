﻿namespace Trabalho_Bruno.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.btnCadastrar = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnAgendar = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnBuscarAgendamento = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnBuscarCliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarAgendamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCadastrar.color = System.Drawing.Color.DodgerBlue;
            this.btnCadastrar.colorActive = System.Drawing.Color.SteelBlue;
            this.btnCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCadastrar.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.btnCadastrar.ForeColor = System.Drawing.Color.White;
            this.btnCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCadastrar.Image")));
            this.btnCadastrar.ImagePosition = 20;
            this.btnCadastrar.ImageZoom = 50;
            this.btnCadastrar.LabelPosition = 41;
            this.btnCadastrar.LabelText = "Cadastrar Cliente";
            this.btnCadastrar.Location = new System.Drawing.Point(115, 42);
            this.btnCadastrar.Margin = new System.Windows.Forms.Padding(6);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(129, 129);
            this.btnCadastrar.TabIndex = 7;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // btnAgendar
            // 
            this.btnAgendar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAgendar.color = System.Drawing.Color.DodgerBlue;
            this.btnAgendar.colorActive = System.Drawing.Color.SteelBlue;
            this.btnAgendar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgendar.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.btnAgendar.ForeColor = System.Drawing.Color.White;
            this.btnAgendar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgendar.Image")));
            this.btnAgendar.ImagePosition = 20;
            this.btnAgendar.ImageZoom = 50;
            this.btnAgendar.LabelPosition = 41;
            this.btnAgendar.LabelText = "Agendar";
            this.btnAgendar.Location = new System.Drawing.Point(253, 200);
            this.btnAgendar.Margin = new System.Windows.Forms.Padding(6);
            this.btnAgendar.Name = "btnAgendar";
            this.btnAgendar.Size = new System.Drawing.Size(129, 129);
            this.btnAgendar.TabIndex = 10;
            this.btnAgendar.Click += new System.EventHandler(this.btnAgendar_Click);
            // 
            // btnBuscarAgendamento
            // 
            this.btnBuscarAgendamento.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBuscarAgendamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnBuscarAgendamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscarAgendamento.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarAgendamento.Image")));
            this.btnBuscarAgendamento.ImageActive = null;
            this.btnBuscarAgendamento.Location = new System.Drawing.Point(391, 200);
            this.btnBuscarAgendamento.Name = "btnBuscarAgendamento";
            this.btnBuscarAgendamento.Size = new System.Drawing.Size(54, 48);
            this.btnBuscarAgendamento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnBuscarAgendamento.TabIndex = 11;
            this.btnBuscarAgendamento.TabStop = false;
            this.btnBuscarAgendamento.Zoom = 10;
            this.btnBuscarAgendamento.Click += new System.EventHandler(this.btnBuscarAgendamento_Click);
            // 
            // btnBuscarCliente
            // 
            this.btnBuscarCliente.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBuscarCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnBuscarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarCliente.Image")));
            this.btnBuscarCliente.ImageActive = null;
            this.btnBuscarCliente.Location = new System.Drawing.Point(253, 42);
            this.btnBuscarCliente.Name = "btnBuscarCliente";
            this.btnBuscarCliente.Size = new System.Drawing.Size(54, 48);
            this.btnBuscarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnBuscarCliente.TabIndex = 12;
            this.btnBuscarCliente.TabStop = false;
            this.btnBuscarCliente.Zoom = 10;
            this.btnBuscarCliente.Click += new System.EventHandler(this.btnBuscarCliente_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(351, -23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(244, 217);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(556, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Trabalho_Bruno.Properties.Resources._240_F_198851683_6LHMXfNiNcs47JTMK25fvN4gPfs2FQ3B;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(592, 377);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBuscarCliente);
            this.Controls.Add(this.btnBuscarAgendamento);
            this.Controls.Add(this.btnAgendar);
            this.Controls.Add(this.btnCadastrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Principal";
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarAgendamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuTileButton btnCadastrar;
        private Bunifu.Framework.UI.BunifuTileButton btnAgendar;
        private Bunifu.Framework.UI.BunifuImageButton btnBuscarAgendamento;
        private Bunifu.Framework.UI.BunifuImageButton btnBuscarCliente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}