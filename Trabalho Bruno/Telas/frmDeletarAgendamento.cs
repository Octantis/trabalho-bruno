﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmDeletarAgendamento : Form
    {
        public frmDeletarAgendamento()
        {
            InitializeComponent();
        }

        public void CarregarTela(Model.Agendamento_ClienteModel model)
        {
            txtIdAgendamento.Text = model.IDAgendamento.ToString();
        }

        private void Deletar()
        {
            try
            {
                int id = Convert.ToInt32(txtIdAgendamento.Text);

                Business.AgendamentoBusiness Business = new Business.AgendamentoBusiness();
                Business.DeletarAgendamento(id);

                MessageBox.Show("Agendamento Removido com sucesso");

                txtNomeCli.Text = string.Empty;
                dtpInicio.Value = DateTime.Now;
                dtpFim.Value = DateTime.Now;
                txtInicio.Text = string.Empty;
                txtFim.Text = string.Empty;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtIdAgendamento_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int idAgendamento = Convert.ToInt32(txtIdAgendamento.Text);

                Business.Agendamento_ClienteBusiness BusinessAG = new Business.Agendamento_ClienteBusiness();
                Model.Agendamento_ClienteModel Modelo = BusinessAG.FiltrarPorId(idAgendamento);

                if (Modelo != null)
                {
                    txtNomeCli.Text = Modelo.cliente;
                    dtpInicio.Value = Modelo.dataInicio;
                    dtpFim.Value = Modelo.dataFim;
                    txtInicio.Text = Convert.ToString(Modelo.chegada);
                    txtFim.Text = Convert.ToString(Modelo.saida);
                }
                else
                {
                    txtNomeCli.Text = string.Empty;
                    dtpInicio.Value = DateTime.Now;
                    dtpFim.Value = DateTime.Now;
                    txtInicio.Text = string.Empty;
                    txtFim.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("ID nao pode estar vazio");

                txtNomeCli.Text = string.Empty;
                dtpInicio.Value = DateTime.Now;
                dtpFim.Value = DateTime.Now;
                txtInicio.Text = string.Empty;
                txtFim.Text = string.Empty;
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            this.Deletar();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtIdAgendamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Deletar();
            }
        }
    }
}
