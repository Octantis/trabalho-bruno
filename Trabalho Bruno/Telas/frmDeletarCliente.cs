﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno.Telas
{
    public partial class frmDeletarCliente : Form
    {
        public frmDeletarCliente()
        {
            InitializeComponent();
        }

        public void CarregarTela(Model.ClienteModel model)
        {
            txtIddeletar.Text = model.IDCliente.ToString();
        }

        private void Deletar()
        {
            try
            {
                int id = Convert.ToInt32(txtIddeletar.Text);

                Business.ClienteBusiness Business = new Business.ClienteBusiness();
                Business.Deletar(id);

                MessageBox.Show("Cliente Removido com sucesso");

                txtNome.Text = string.Empty;
                txtCPF.Text = string.Empty;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtIddeletar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtIddeletar.Text);
                Business.ClienteBusiness Business = new Business.ClienteBusiness();
                Model.ClienteModel Modelo = Business.BuscarId(id);

                if (Modelo != null)
                {
                    txtNome.Text = Modelo.cliente;
                    txtCPF.Text = Modelo.cpf;
                }
                else
                {
                    txtNome.Text = string.Empty;
                    txtCPF.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("ID deve ser preenchido");
                txtNome.Text = string.Empty;
                txtCPF.Text = string.Empty;
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            this.Deletar();
        }

        private void txtIddeletar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Deletar();
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
