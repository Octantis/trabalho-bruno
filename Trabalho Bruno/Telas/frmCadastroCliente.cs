﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_Bruno
{
    public partial class frmCadastroCliente : Form
    {
        public frmCadastroCliente()
        {
            InitializeComponent();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VerificarIdade()
        {
            DateTime nascimento = dtpNascimento.Value;
            DateTime atual = DateTime.Now;
            int idade = atual.Year - nascimento.Year;

            if (idade < 18)
            {
                btnEntrar.Enabled = false;
            }
            else
            {
                btnEntrar.Enabled = true;
            }
        }

        private void dtpNascimento_onValueChanged(object sender, EventArgs e)
        {
            this.VerificarIdade();
        }

        private void Cadastrar()
        {
            try
            {
                Model.ClienteModel model = new Model.ClienteModel();
                model.cliente = txtNome.Text;
                model.cpf = txtCPF.Text;
                model.email = txtEmail.Text;
                model.nasc = dtpNascimento.Value.Date;
                model.tel = txtTelefone.Text;
                model.celular = txtCelular.Text;

                DateTime nascimento = dtpNascimento.Value;
                DateTime atual = DateTime.Now;
                int idade = atual.Year - nascimento.Year;
                string cpf = txtCPF.Text;

                if (idade < 18)
                {
                    MessageBox.Show("Menor de 18 anos");
                }
                else
                {
                    if(txtNome.Text == string.Empty)
                    {
                        MessageBox.Show("Informe o nome do cliente");
                    }
                    else if (cpf.Length < 14)
                    {
                        MessageBox.Show("CPF invalido");
                    }
                    else if (model.email.Contains("@"))
                    {
                        Business.ClienteBusiness clientebusiness = new Business.ClienteBusiness();
                        clientebusiness.InserirCliente(model);

                        MessageBox.Show("Cliente inserido com sucesso");

                        frmAgendamento tela = new frmAgendamento();
                        tela.Show();

                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Email invalido");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnEntrar_Click_1(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Cadastrar();
            }
        }
    }
}
